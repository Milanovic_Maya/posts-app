import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import ListComponent from "../containers/ListComponent";
import Login from "../containers/Login";
import createHistory from "history/createBrowserHistory";
// import PrivateRoute from "./PrivateRoute";
// import PublicRoute from "./PublicRoute";
import ItemDetails from "../containers/ItemDetails";
import AddItemForm from "../containers/AddItemForm";


export const history = createHistory();

const AppRouter = props => {
  return (
    <div>
      <Router history={history}>
        <Switch>
          {/* <PublicRoute path="/login" component={Login} />
              <PrivateRoute path="/" component={ListComponent} />*/}
          <Route path="/login" component={Login} />
          <Route path="/details/:id" component={ItemDetails} />
          <Route path="/create" component={AddItemForm} />
          <Route path="/" component={ListComponent} />
        </Switch>
      </Router>
    </div>
  );
};

export default AppRouter;
