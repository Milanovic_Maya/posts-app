import { createStore, combineReducers, applyMiddleware } from "redux";
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import listReducer from "../reducers/listReducer";
import filterReducer from "../reducers/filterReducer";
import authReducer from "../reducers/authReducer";
import detailsReducer from "../reducers/detailsReducer";

const logger = createLogger();

export default () => {
    const store = createStore(
        combineReducers({
            list: listReducer,
            filters: filterReducer,
            auth: authReducer,
            itemDetails: detailsReducer
        }), applyMiddleware(thunk, logger)
    );
    return store;
};
