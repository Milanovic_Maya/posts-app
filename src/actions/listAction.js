// import Item from "../entities/Item";
// import { BASE_URL, KEY } from "../utils/constants";
import database from "../firebase/firebase";
// import data from "../utils/db";

export const SET_LIST = "SET_LIST";
export const DELETE_ITEM = "DELETE_ITEM";
export const EDIT_ITEM = "EDIT_ITEM";
export const ADD_ITEM = "ADD_ITEM";


export const setList = list => ({
    type: SET_LIST,
    list
});

// export const startSetInitialList = () => {
//     return (dispatch, getState) => {
//         const uid = getState().auth.uid;
//         // return fetch(`${BASE_URL}${KEY}`)
//         //     .then(res => res.json())
//         //     .then(data => {
//         const itemList = data().slice(0, 10).map(item => new Item(item))

//         return database.ref(`users/${uid}/items`).set(itemList)
//             .then(ref => {
//                 return dispatch(setList(itemList))
//             })
//         // })
//         // .catch(error => new Error(`Error happened! ${error.message}`))
//     };
// };

export const startSetList = () => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/items`).once('value', snapshot => {
            const itemList = [];
            snapshot.forEach((childSnapshot) => {
                itemList.push({
                    id: childSnapshot.key,
                    ...childSnapshot.val()
                });
            });
            dispatch(setList(itemList));
        });
    };
};

export const startSetSortedList = list => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/items`).set(list)
            .then(ref => {
                return dispatch(setList(list))
            })
            .catch(error => console.log(error.message))
    };
};

export const deleteAction = ({
    id
} = {}) => ({
    type: DELETE_ITEM,
    id
});

export const startDeleteItem = ({
    id
} = {}) => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/items/${id}`).remove().then(() => {
            dispatch(deleteAction({
                id
            }));
        });
    };
};

export const editItem = (id, updates) => ({
    type: EDIT_ITEM,
    id,
    updates
});

export const startEditItem = (id, updates) => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/items/${id}`).update(updates).then(() => {
            dispatch(editItem(id, updates));
        });
    };
};

export const addItem = item => ({
    type: ADD_ITEM,
    item
});

export const startAddItem = (itemData = {}) => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        const { name = "", text = "", dateModified = 0, dateCreated = 0, amount = 0 } = itemData;
        const item = { name, text, dateModified, dateCreated, amount };

        return database.ref(`users/${uid}/items`).push(item)
            .then(ref => {
                console.log(ref);
                
                return dispatch(addItem({
                    id: ref.key,
                    ...item
                }));
            })
    };
};