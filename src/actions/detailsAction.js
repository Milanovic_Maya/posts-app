import database from "../firebase/firebase";

export const ITEM_DETAILS = "ITEM_DETAILS";


export const detailsAction = item => ({
    type: ITEM_DETAILS,
    itemDetails: item
});

export const startDetailsAction = (item) => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/selectedItem`).set(item)
            .then(() => {
                return dispatch(detailsAction(item));
            })
            .catch(error => console.log(error.message))
    }
};

