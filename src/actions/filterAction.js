import database from "../firebase/firebase";

export const SORT = "SORT";

export const sortBy = sort => ({
    type: SORT,
    sortBy: sort
});

export const saveSortByFilterForUser = sort => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        database
            .ref(`users/${uid}`)
            .set({
                sortBy: sort
            })
            .then(() => {
                dispatch(sortBy(sort));
            });
    };
};

export const setSortBy = () => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database
            .ref(`users/${uid}/sortBy`)
            .once("value")
            .then(snapshot => {
                dispatch(sortBy(snapshot.val()));
            });
    };
};