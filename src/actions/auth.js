import firebase from 'firebase/app';
import { googleAuthProvider, gitHubAuthProvider } from "../firebase/firebase";

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

export const login = uid => ({
    type: LOGIN,
    uid
});
export const logout = () => ({
    type: LOGOUT
});

export const startGoogleLogin = () => {
    return () => firebase.auth().signInWithPopup(googleAuthProvider);
};

export const startGitHubLogin = () => {
    return () => firebase.auth().signInWithPopup(gitHubAuthProvider);
};

export const startLogout = () => {
    return () => {
        return firebase.auth().signOut()
    }
};