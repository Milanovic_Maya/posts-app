class Item {
    constructor(item) {
        this.name = item.title;
        this.text = item.description;
        this.amount = parseFloat(item.price, 0);
        this.id = item.id;
        this.dateCreated = item.createdAt;
        this.dateModified = item.updatedAt;
        this.authorID=item.authorID;
    }
}

export default Item;