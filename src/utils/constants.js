export const BASE_URL = "https://my.api.mockaroo.com/list_items.json?key=";
export const KEY = "16d899b0";
export const DEFAULT_USER_IMG = "https://cdn1.iconfinder.com/data/icons/instagram-ui-colored/48/JD-18-512.png";
export const LOADING_MESSAGE = "Loading items...";

export const loginText = {
    gitHub: "Login with GitHub account",
    google: "Login with Google account",
    guest: "Continue as Guest"
};

export const message = user => {
    const withUser = !user ? " login and " : "";
    const emptyList = `Currently there are no items in list. Please ${withUser} start creating your list items.`;
    return emptyList;
};