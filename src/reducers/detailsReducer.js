import { ITEM_DETAILS } from "../actions/detailsAction";

const defaultState = {
    name: "",
    text: "",
    amount: "",
    id: "",
    dateCreated: "",
    dateModified: ""
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case ITEM_DETAILS:
            return action.itemDetails;
        default:
            return state;
    }
}