// reducer is pure function that updates store
import { SET_LIST, DELETE_ITEM, EDIT_ITEM, ADD_ITEM } from "../actions/listAction";

const defaultState = [];

const listReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_LIST:
            return action.list;
        case ADD_ITEM:
            return [
                ...state,
                action.item
            ]
        case DELETE_ITEM:
            return state.filter(({ id }) => id !== action.id);
        case EDIT_ITEM:
            return state.map((item) => {
                if (item.id === action.id) {
                    return {
                        ...item,
                        ...action.updates
                    };
                } else {
                    return item;
                }
            });
        default:
            return state;
    }
};
export default listReducer;