import {
    SORT,
} from "../actions/filterAction";

const filterDefaultState = {
    sortBy: "dateCreated",
};

export default (state = filterDefaultState, action) => {
    switch (action.type) {
        case SORT:
            return {
                ...state,
                sortBy: action.sortBy
            };
        default:
            return state;
    }
};