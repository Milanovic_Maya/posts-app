import React from "react";
import ListItem from "./ListItem";
import { connect } from "react-redux";
import Filter from "./Filter";
import filterItemList from "../selectors/filterItemList";
import Button from '@material-ui/core/Button';
import { startLogout } from "../actions/auth";
import AddItem from "../components/AddItem";
import Message from "../components/partials/Message";

const ListComponent = ({
    list,
    sortBy,
    history,
    startLogout,
    userStatus }) => {
    const onButtonClick = e => {
        if (userStatus) {
            startLogout()
                .catch(error => console.log(error.message))
        } else if (!userStatus) {
            history.push("/login");
        }
    };

    return (
        <div>
            <div className="container">
                <div className="logout-button__wrapper">
                    <Button
                        size="small"
                        color="primary"
                        onClick={onButtonClick}
                    >
                        {userStatus ? "Logout" : "Login"}
                    </Button>
                </div>
                {list.length === 0 ? <Message user={userStatus} />
                    :
                    <div>
                        <h3>ITEMS:</h3>
                        {userStatus &&
                            <div className="filter-input__wrapper">
                                <Filter />
                            </div>}
                        <ul>
                            {list && list.map(item => {
                                return (
                                    <li key={`${item.name}/${item.id}`}>
                                        <ListItem item={item} />
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                }
            </div>
            {userStatus && <AddItem />}
        </div>
    );
};

const mapStateToProps = state => ({
    list: filterItemList(state.list, state.filters),
    userStatus: !!state.auth.uid,
    sortBy: state.filters.sortBy
});

const mapDispatchToProps = dispatch => ({
    startLogout: () => dispatch(startLogout()),
})

export default connect(mapStateToProps, mapDispatchToProps)(ListComponent);