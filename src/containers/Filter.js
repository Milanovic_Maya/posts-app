import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
// import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { saveSortByFilterForUser } from "../actions/filterAction";

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        width: "100%"
    },
    formControl: {
        minWidth: 120,
        width: "60%"
    },
    select: {
        marginTop: theme.spacing.unit * 2,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    textField: {
        width: "60%"
    }
});

class Filter extends Component {
    onSortChange = e => {
        const sortBy = e.target.value;

        this.props.saveSortByFilterForUser(sortBy);
    };

    render() {
        const { classes } = this.props;
        return (
            <form className={classes.root} autoComplete="off">
              {/*  <TextField
                    id="filter"
                    label="Filter by title"
                    className={classes.textField}
                    value={this.props.filters.text}
                    onChange={this.onTextChange}
                    margin="normal"
              />*/}
                <FormControl className={classes.formControl}>
                    <Select
                        className={classes.select}
                        value={this.props.sortBy}
                        onChange={this.onSortChange}
                        inputProps={{
                            name: 'sortBy',
                            id: 'sortBy-simple',
                        }}
                    >
                        <MenuItem value={"dateCreated"}>byDateCreated</MenuItem>
                        <MenuItem value={"dateModified"}>ByDateModified</MenuItem>
                        <MenuItem value={"amount"}>ByAmount</MenuItem>
                    </Select>
                    <FormHelperText>Select parameter to sort items</FormHelperText>
                </FormControl>
            </form>
        );

    };
};

const mapStateToProps = (state) => ({
    sortBy: state.filters.sortBy
});

const mapDispatchToProps = dispatch => ({
    saveSortByFilterForUser: sortBy => dispatch(saveSortByFilterForUser(sortBy))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Filter));