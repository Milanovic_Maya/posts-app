import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import { DEFAULT_USER_IMG, loginText } from "../utils/constants";
import { connect } from "react-redux";
import { startGoogleLogin, startGitHubLogin } from "../actions/auth";
import OctocatIcon from "../components/partials/OctocatIcon";
import GoogleIcon from "../components/partials/GoogleIcon";

const styles = theme => ({
    card: {
        background: "#ECEFF1",
        maxWidth: 600,
        padding: "10%"
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    },
    icon: {
        marginRight: theme.spacing.unit
    }
});


class Login extends Component {
    onSignInWithGoogle = e => {
        this.props.startGoogleLogin()
            .then(() => this.props.history.push("/"))
            .catch(error => console.log(error.message))
    };

    onSignInWithGitHub = e => {
        this.props.startGitHubLogin()
            .then(() => this.props.history.push("/"))
            .catch(error => console.log(error.message))
    };

    onGuestClick = e => {
        this.props.history.push("/");
    };


    render() {
        const { classes } = this.props;
        return (
            <div className="container__login">
                <Card className={classes.card}>
                    <CardMedia
                        className={classes.media}
                        image={DEFAULT_USER_IMG}
                        title="Contemplative Reptile"
                    />
                    <div className="login-buttons__wrapper">
                        <Button
                            fullWidth
                            size="small"
                            color="primary"
                            onClick={this.onSignInWithGoogle}
                        >
                            <GoogleIcon className={classes.icon} />
                            {loginText.google}
                        </Button>
                        <Button
                            fullWidth
                            size="small"
                            color="primary"
                            onClick={this.onSignInWithGitHub}
                        >
                            <OctocatIcon className={classes.icon} />
                            {loginText.gitHub}
                        </Button>
                        <Button
                            fullWidth
                            size="small"
                            color="primary"
                            onClick={this.onGuestClick}
                        >
                            {loginText.guest}
                        </Button>
                    </div>
                </Card>
            </div>
        );
    };
};

const mapDispatchToProps = dispatch => ({
    startGoogleLogin: () => dispatch(startGoogleLogin()),
    startGitHubLogin: () => dispatch(startGitHubLogin())
})

export default connect(null, mapDispatchToProps)((withStyles(styles)(Login)));