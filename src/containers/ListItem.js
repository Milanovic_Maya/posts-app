import React from 'react';
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { startDetailsAction } from "../actions/detailsAction";

const styles = {
    card: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        marginBottom: 16,
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
};

const ListItem = props => {
    const { classes, item, startDetailsAction, history } = props;
    const onDetailsClick = e => {
        startDetailsAction(item)
            .then(() => history.push(`/details/${item.id}`))
    };
    return (
        <div>
            <Card className={classes.card}>
                <CardContent>
                    <Typography className={classes.title} color="textSecondary">
                        {`${item.name}....created on:${item.dateCreated}`}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button
                        fullWidth
                        size="small"
                        color="primary"
                        onClick={onDetailsClick}
                    >
                        <span>See Details >></span>
                    </Button>
                </CardActions>
            </Card>
        </div>
    );
}

const mapDispatchToProps = dispatch => ({
    startDetailsAction: item => dispatch(startDetailsAction(item))
})

export default connect(null, mapDispatchToProps)(withStyles(styles)(withRouter(ListItem)));