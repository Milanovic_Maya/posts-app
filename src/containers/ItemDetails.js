import React, { Component } from 'react';
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import { startDeleteItem, startEditItem } from "../actions/listAction";
import database from "../firebase/firebase";
import moment from "moment";
import GuestItemDetails from '../components/GuestItemDetails';
import ItemDetailsButtons from '../components/ItemDetailsButtons';
import UpdateItem from '../components/UpdateItem';

const styles = theme => ({
    card: {
        minWidth: 275,
    }
});

class ItemDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            name: '',
            amount: '',
            dateModified: "",
            selectedItem: null
        };
    }

    onUpdateClick = e => {
        let { selectedItem, text, name, amount, dateModified, id } = this.state;
        dateModified = moment().format("MMM Do YYYY");
        if (selectedItem) {
            this.props.startEditItem(id, { text, name, amount, dateModified })
                .then(() => this.props.history.push("/"))
        }
    };

    onDeleteClick = e => {
        const { id } = this.state;
        this.props.startDeleteItem({ id })
            .then(() => this.props.history.push("/"))
    };

    onNameChange = e => {
        const name = e.target.value;
        this.setState(() => ({ name }));
    };
    onTextChange = e => {
        const text = e.target.value;
        this.setState(() => ({ text }));
    };

    onAmountChange = e => {
        const amount = e.target.value;
        this.setState(() => ({ amount }));
    };
    onBackClick = e => {
        this.props.history.push("/");
    };


    componentDidMount() {
        const { uid } = this.props;

        // fetch selected item from firebase because store is not persistent;

        database.ref(`users/${uid}/selectedItem`).once("value")
            .then((snapshot) => this.setState(() => ({
                text: snapshot.val().text,
                id: snapshot.val().id,
                name: snapshot.val().name,
                amount: snapshot.val().amount,
                dateModified: snapshot.val().dateModified,
                selectedItem: snapshot.val()
            })))
            .catch(error => console.log(error.message))
    };

    render() {
        const { classes,
            userStatus,
        } = this.props;
        const { name, text, amount, selectedItem } = this.state;
        const { uid } = this.props;
        return (
            <div className="container">
                <Button
                    size="small"
                    onClick={this.onBackClick}
                >
                    {`<< Back`}
                </Button>
                <Card className={classes.card}>
                    <CardContent>
                        {!userStatus && <GuestItemDetails selectedItem={selectedItem} />}
                        {selectedItem && userStatus &&
                            <div>
                                <h4>{name}</h4>
                                <p>{text}</p>
                            </div>}
                        {selectedItem && (uid === selectedItem.authorID) &&
                            <div>
                                <UpdateItem
                                    onNameChange={this.onNameChange}
                                    name={name}
                                    text={text}
                                    amount={amount}
                                    onTextChange={this.onTextChange}
                                    onAmountChange={this.onAmountChange}
                                />

                                <ItemDetailsButtons
                                    onDeleteClick={this.onDeleteClick}
                                    onUpdateClick={this.onUpdateClick}
                                />
                            </div>
                        }
                    </CardContent>
                </Card>
            </div>
        );

    };
};

const mapStateToProps = state => ({
    userStatus: !!state.auth.uid,
    uid: state.auth.uid
});
const mapDispatchToProps = dispatch => ({
    startDeleteItem: ({ id }) => dispatch(startDeleteItem({ id })),
    startEditItem: (id, item) => dispatch(startEditItem(id, item))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ItemDetails));
