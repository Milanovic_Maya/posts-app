import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddButton from "../components/partials/AddButton";
import { connect } from "react-redux";
import { startAddItem } from "../actions/listAction";
import moment from "moment";
import Item from '../entities/Item';

class AddItemForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            title: "",
            description: "",
            price: "",
        };
    }

    clearFormInputs = () => {
        this.setState(() => ({
            title: "",
            description: "",
            price: ""
        }));
    };

    handleClickOpen = () => {
        this.setState(() => ({
            open: true
        }))
    };

    handleClose = () => {
        this.setState(() => ({
            open: false
        }));
        this.clearFormInputs();
    };
    onTitleChange = e => {
        const title = e.target.value;
        this.setState(() => ({ title }));
    };
    onTextChange = e => {
        const description = e.target.value;
        this.setState(() => ({ description }));
    };
    onPriceChange = e => {
        const price = e.target.value;
        this.setState(() => ({ price }));
    };

    onCreateItem = e => {
        const { title, price, description } = this.state;
        if (title && price && description) {
            const itemData = {
                title,
                price,
                description,
                createdAt: moment().format("MMMM Do YYYY, h:mm:ss a"),
                updatedAt: moment().format("MMMM Do YYYY, h:mm:ss a")
            };
            const item = new Item(itemData);

            this.props.startAddItem(item)
                .then(() => {
                    this.setState({ open: false });
                    this.clearFormInputs();
                })
                .catch(error => console.log(error.message))
        };
    };

    render() {
        return (
            <div>
                <AddButton onOpenClick={this.handleClickOpen} />
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Create Item</DialogTitle>
                    <DialogContent>
                        <TextField
                            id="title"
                            label="Title"
                            value={this.state.title}
                            onChange={this.onTitleChange}
                            fullWidth
                            autoFocus
                            autoComplete="false"
                        />
                        <TextField
                            value={this.state.text}
                            onChange={this.onTextChange}
                            id="text"
                            label="Text"
                            fullWidth
                            multiline={true}
                            autoComplete="false"
                        />
                        <TextField
                            id="number"
                            label="amount"
                            value={this.state.price}
                            onChange={this.onPriceChange}
                            type="number"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            margin="normal"
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                    </Button>
                        <Button
                            onClick={this.onCreateItem}
                            color="primary"
                            disabled={
                                !this.state.title || !this.state.price || !this.state.description
                            }
                        >
                            Create
                    </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    startAddItem: item => dispatch(startAddItem(item))
});
export default connect(null, mapDispatchToProps)(AddItemForm);
