import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const config = {
    apiKey: "AIzaSyC7gbBGRzVtWWWPL9KWUZA2E-FLXamZcHo",
    authDomain: "list-app-c7635.firebaseapp.com",
    databaseURL: "https://list-app-c7635.firebaseio.com",
    projectId: "list-app-c7635",
    storageBucket: "",
    messagingSenderId: "817369520588"
};

firebase.initializeApp(config);

const database = firebase.database();

// provider for Google
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
googleAuthProvider.setCustomParameters({
    prompt: 'select_account'
});

// provider for GitHub

const gitHubAuthProvider = new firebase.auth.GithubAuthProvider();

export {
    firebase,
    googleAuthProvider,
    gitHubAuthProvider,
    database as
    default
};