import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter, { history } from './routes/AppRouter';
import "normalize.css";
import "./styles/css/styles.css";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import { /*startSetInitialList,*/ startSetList } from './actions/listAction';
import firebase from 'firebase/app';
import { login, logout } from "./actions/auth";
import Spinner from './components/partials/Spinner';

const store = configureStore();

const App = () => (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);

let hasRendered = false;
const renderApp = () => {
    if (!hasRendered) {
        ReactDOM.render(<App />, document.getElementById('root'));
        hasRendered = true;
    }
};

ReactDOM.render(<Spinner />, document.getElementById('root'));

firebase.auth().onAuthStateChanged(user => {
    if (user) {
        store.dispatch(login(user.uid));
        store.dispatch(startSetList()).then(() => {
            // if (store.getState().list.length === 0) {
            //     store.dispatch(startSetInitialList());
            // };
            renderApp();
            if (history.location.pathname === '/login') {
                history.push('/');
            }
        });
    } else {
        store.dispatch(logout());
        // if (store.getState().list.length === 0) {
        //     store.dispatch(startSetInitialList());
        // };
        renderApp();
        history.push('/login');
    }
});
