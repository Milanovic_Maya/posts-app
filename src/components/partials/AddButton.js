import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import purple from '@material-ui/core/colors/purple';
import classNames from 'classnames';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    cssRoot: {
        color: theme.palette.getContrastText(purple[500]),
        backgroundColor: purple[500],
        '&:hover': {
            backgroundColor: purple[700],
        },
    }
});

const AddButton = ({ classes, onOpenClick }) => (
    <div className="add-button__wrapper">
        <Button
            variant="fab"
            color="primary"
            aria-label="Add"
            className={classNames(classes.button, classes.cssRoot)}
            onClick={onOpenClick}
        >
            <AddIcon />
        </Button>
    </div>
);

AddButton.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AddButton);
