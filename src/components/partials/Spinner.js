import React from "react";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import purple from "@material-ui/core/colors/purple";
import { LOADING_MESSAGE } from "../../utils/constants";

const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
    },
});

const Spinner = ({ classes }) => {
    return (
        <div className="container__spinner">
            <CircularProgress
                className={classes.progress}
                style={{ color: purple[500] }}
                thickness={4}
                size={100}
            />
            <span className="loading-message">{LOADING_MESSAGE}</span>
        </div>
    );
}

export default withStyles(styles)(Spinner);