import React from "react";
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    btns: {
        margin: "auto",
        width:"80%",
        display:"flex",
        justifyContent:"center"
    }
});

const ItemDetailsButtons = ({ classes, onUpdateClick, onDeleteClick }) => {
    return (
        <CardActions className={classes.btns}>
            <Button
                size="small"
                onClick={onUpdateClick}
            >
                update
            </Button>
            <Button
                size="small"
                onClick={onDeleteClick}
            >
                delete
            </Button>
        </CardActions>
    );
};

export default withStyles(styles)(ItemDetailsButtons);