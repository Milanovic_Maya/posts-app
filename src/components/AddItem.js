import React from "react";
import AddItemForm from "../containers/AddItemForm";

const AddItem = props => {
    return (
        <div>
            <AddItemForm />
        </div>
    );
};

export default AddItem;