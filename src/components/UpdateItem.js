import React from "react";
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    textField: {
        margin: "auto",
        width: "80%",
    }
});

const UpdateItem = props => {
    const { name, text, amount, classes, onNameChange, onTextChange, onAmountChange } = props;
    return (
        <div className="item-details__inputs-wrapper">
            <TextField
                id="name"
                label="Name"
                className={classes.textField}
                value={name}
                onChange={onNameChange}
                margin="normal"
                autoFocus={true}
            />
            <TextField
                id="text"
                label="text"
                className={classes.textField}
                value={text}
                onChange={onTextChange}
                margin="normal"
                multiline={true}
            />
            <TextField
                id="number"
                label="amount"
                value={amount}
                onChange={onAmountChange}
                type="number"
                className={classes.textField}
                InputLabelProps={{
                    shrink: true,
                }}
                margin="normal"
            />
        </div>
    );
};

export default withStyles(styles)(UpdateItem);