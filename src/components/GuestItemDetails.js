import React from "react";
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    title: {
        marginBottom: 16,
        fontSize: 14,
    }
});

const GuestItemDetails = ({ selectedItem, classes }) => {
    return (
        <div>
            <Typography className={classes.title} color="textSecondary">
                {selectedItem && selectedItem.name}
            </Typography>
            <Typography component="p">
                {selectedItem && selectedItem.text}
                <br />
                {selectedItem && `Amount: ${selectedItem.amount}`}
            </Typography>
        </div>
    );
};

export default withStyles(styles)(GuestItemDetails);