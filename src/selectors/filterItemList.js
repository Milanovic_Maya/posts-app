export default (list, { sortBy }) => {
  return list.sort((x, y) => {
    switch (sortBy) {
      case "dateCreated":
        return x.dateCreated < y.dateCreated ? 1 : -1;
      case "dateModified":
        return x.dateModified < y.dateModified ? 1 : -1;
      case "amount":
        return x.amount < y.amount ? 1 : -1;
      default:
        return 0;
    }
  });
};
